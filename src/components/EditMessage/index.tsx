import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { IMessage, EditCallbacks } from '../../containers/Chat/types';

import styles from './styles.module.scss';

type EditProps = { message: IMessage } & EditCallbacks

const EditMessage = ({ message, toggle, apply }: EditProps) => {
  const [text, setText] = useState(message.text);
  const modalStyleName = `${styles.modal} modal`;

  const handleSave = (toSend: string) => {
    apply(toSend);
    toggle(false);
  };
  return (
    <div className={modalStyleName} tabIndex={-1} role="dialog">
      <div className="modal-dialog" role="document">
        <div className="modal-content">
          <div className="modal-body">
            <textarea
              autoFocus
              className={styles.messageText}
              value={text}
              placeholder="Write a message"
              onChange={(ev: React.ChangeEvent<HTMLTextAreaElement>) => {
                setText(ev.target.value);
              }}
            />
          </div>
          <div className="modal-footer">
            <button
              type="button"
              className="btn btn-primary"
              onClick={() => handleSave(text)}
            >
              Save changes
            </button>
            <button
              type="button"
              className="btn btn-secondary"
              data-dismiss="modal"
              onClick={() => toggle(false)}
            >
              Close
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

EditMessage.propTypes = {
  message: PropTypes.objectOf(PropTypes.any).isRequired,
  toggle: PropTypes.func.isRequired,
  apply: PropTypes.func.isRequired
};

export default EditMessage;
