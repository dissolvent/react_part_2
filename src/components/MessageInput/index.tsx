import React, { useState } from 'react';
import PropTypes from 'prop-types';

import styles from './styles.module.scss';

type MessageInputProps = {
  sendMessage: (message: string) => void
}

const MessageInput = ({ sendMessage }: MessageInputProps): JSX.Element => {
  const [toSend, setToSend] = useState<string>('');

  const handleSend = (text: string) => {
    if (!text.trim().length) {
      return;
    }
    sendMessage(text);
    setToSend('');
  };

  return (
    <div className={styles.inputWrap}>
      <textarea
        className={styles.inputArea}
        value={toSend}
        placeholder="Write a message"
        onChange={(ev: React.ChangeEvent<HTMLTextAreaElement>) => {
          setToSend(ev.target.value);
        }}
      />
      <button
        type="button"
        className={styles.sendMessage}
        onClick={() => handleSend(toSend)}
      >
        &#8657; Send
      </button>
    </div>
  );
};

MessageInput.propTypes = {
  sendMessage: PropTypes.func.isRequired
};

export default MessageInput;
