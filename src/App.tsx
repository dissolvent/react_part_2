import React from 'react';
import Chat from './containers/Chat';
import Header from './components/Header';
import Footer from './components/Footer';

const App = () => (
  <React.StrictMode>
    <Header />
    <Chat />
    <Footer />
  </React.StrictMode>
);

export default App;
