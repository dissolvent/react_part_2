export interface ISendMessage {
  text: string
}

export interface IMessage {
  id: string
  text: string
  user: string
  avatar?: string
  userId: string
  editedAt: string
  createdAt: string
  canEdit: boolean
  likesCount: number
  myMessage: boolean
}
export interface MessageCallbacks {
  likeMessage: (id: string) => void
  deleteMessage: (id: string) => void
  toggleEdit: (toggle: boolean) => void
}

export interface EditCallbacks {
  toggle: (toggle: boolean) => void
  apply: (text: string) => void
}

export interface IFetchedMessages {
  id: string
  text: string
  user: string
  avatar: string
  userId: string
  editedAt: string
  createdAt: string
}

export interface IProfile {
  userId: string
  user: string
}

export interface IChatState {
  messages: IMessage[]
  isLoading: boolean
  profile: IProfile
  inEditMessage?: IMessage
}
