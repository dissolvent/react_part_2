import { SET_MESSAGES, SEND_MESSAGE, IActionTypes, EDIT_MESSAGE } from './actionTypes';
import { IChatState } from './types';

const initialState: IChatState = {
  messages: [],
  isLoading: true,
  profile: { user: 'Doc', userId: 'bla-bla-bla' }
};

export default (state = initialState, action: IActionTypes) => {
  switch (action.type) {
    case SET_MESSAGES:
      return {
        ...state,
        messages: action.messages,
        isLoading: false
      };
    case SEND_MESSAGE: {
      return {
        ...state,
        messages: [...(state.messages || []), action.payload]
      };
    }
    case EDIT_MESSAGE: {
      return {
        ...state,
        inEditMessage: action.payload
      };
    }
    default: {
      return state;
    }
  }
};
