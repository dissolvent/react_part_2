import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import ChatHeader from '../../components/ChatHeader';
import MessageList from '../../components/MessageList';
import MessageInput from '../../components/MessageInput';
import Spinner from '../../components/Spinner';
import EditMessage from '../../components/EditMessage';
import {
  loadMessages,
  sendMessage,
  likeMessage,
  deleteMessage,
  toggleEditMessage,
  editMessage } from './actions';
import { IChatState, IMessage } from './types';

import styles from './styles.module.scss';

type ChatProps = {
  messages?: IMessage[]
  isLoading: boolean
  inEditMessage: IMessage | undefined
  toggleEditMessage: (toggle: boolean) => void
  loadMessages: () => void
  sendMessage: (text: string) => void
  likeMessage: (id: string) => void
  deleteMessage: (id: string) => void
  editMessage: (text: string) => void
}

const Chat: React.FC<ChatProps> = ({
  messages = [],
  isLoading,
  inEditMessage,
  toggleEditMessage: toggleEditAction,
  loadMessages: loadMessageAction,
  sendMessage: sendMessageAction,
  likeMessage: likeMessageAction,
  deleteMessage: deleteMessageAction,
  editMessage: editMessageAction
}): JSX.Element => {
  useEffect(() => {
    if (isLoading) {
      loadMessageAction();
    }
  }, [isLoading, loadMessageAction]);

  const handleKeyPress = (event: React.KeyboardEvent<HTMLDivElement>) => {
    if (event.key === 'ArrowUp') {
      toggleEditAction(true);
    }
  };

  const lastMessage = () => {
    if (messages.length) {
      return messages ? messages[messages.length - 1].createdAt : '';
    }
    return '';
  };

  return (
    isLoading
      ? <Spinner />
      : (
        <div tabIndex={0} className={styles.chatContainer} onKeyDown={handleKeyPress}>
          <ChatHeader
            name="My awesome chat"
            participants={23}
            messages={messages.length}
            lastMessage={lastMessage()}
          />
          <MessageList
            messages={messages}
            likeMessage={likeMessageAction}
            deleteMessage={deleteMessageAction}
            toggleEdit={toggleEditAction}
          />
          <MessageInput
            sendMessage={sendMessageAction}
          />
          {inEditMessage
            ? (
              <EditMessage
                message={inEditMessage}
                apply={editMessageAction}
                toggle={toggleEditAction}
              />
            )
            : null}
        </div>
      )
  );
};

Chat.propTypes = {
  messages: PropTypes.arrayOf(PropTypes.any),
  isLoading: PropTypes.bool.isRequired,
  inEditMessage: PropTypes.any,
  toggleEditMessage: PropTypes.func.isRequired,
  loadMessages: PropTypes.func.isRequired,
  sendMessage: PropTypes.func.isRequired,
  likeMessage: PropTypes.func.isRequired,
  deleteMessage: PropTypes.func.isRequired,
  editMessage: PropTypes.func.isRequired
};

Chat.defaultProps = {
  messages: [],
  inEditMessage: undefined
};

const mapStateToProps = (state: IChatState) => ({
  messages: state.messages,
  isLoading: state.isLoading,
  inEditMessage: state.inEditMessage
});

const mapDispatchToProps = {
  toggleEditMessage,
  loadMessages,
  sendMessage,
  likeMessage,
  deleteMessage,
  editMessage
};

export default connect(mapStateToProps, mapDispatchToProps)(Chat);
